package model.mario;

/**
 * The four skins of Mario.
 */
public enum MarioType {

    /**
     * The skin when Mario runs.
     */
    RUN,

    /**
     * The skin when Mario jumps.
     */
    JUMP,

    /**
     * The skin when Mario gets the power up and becomes Luigi.
     */
    POWER,

    /**
     * The skin when Luigi jumps.
     */
    POWER_JUMP;
}
