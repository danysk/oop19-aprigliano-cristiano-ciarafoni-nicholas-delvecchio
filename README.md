Super Mario Run is an endless videogame. The purpose of the game is to run and jump over the obstacles that spawns in random order and at random distances between each other.
At the start of each game, the user is requested to choose a Name, which will be used for the Leaderboard. 
The leaderboard shows only the Top 10 scores among all players and games.

Super Mario runs automatically in the same directions and by doing so, it automatically earns points which will be saved in a .txt file at the end of each game.

The Game presents itself with a super intuitive Graphical User Interface with 3 buttons:
"New Game" -> Starts a New Game
"Ranking" -> Shows the Leaderboard of the past games 
"Contacts" -> Shows Name, Surname and Email of the three members of the group 

When starting a New Game, Mario begins to run automatically, and the obstacles start to spawn at Random distances between each other and to pass over the obstacles it is required to press the Space Bar.
We implemented a simple Power-Up (the Iconic Mario's mushroom) which gives you an extra life and transforms you into Luigi. Once you hit an obstacle in Luigi's form, you lose a life and get reverted to Mario's form and if you hit another obstacle, you lose.
By clicking at the "You Lost" prompt message, the game brings you to the view of the Leaderboard and by clicking "Refresh" you'll be able to see all the past scores realized by other players.

If you want to start another game, you have to click on the "Home" button in the Leaderboards view and then click "New Game".


Developed in JAVA by: 

Leonardo Delvecchio - leonardo.delvecchio@studio.unibo.it
Cristiano Aprigliano - cristiano.aprigliano@studio.unibo.it
Nicholas Ciarafoni - nicholas.ciarafoni@studio.unibo.it